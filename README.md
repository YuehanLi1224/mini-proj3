**My S3 bucket:**
![My S3 bucket:](https://gitlab.com/YuehanLi1224/mini-proj3/-/raw/main/Screenshot%20.png?ref_type=heads)

**The bucket has versioning enabled:**
![The bucket has versioning enabled:](https://gitlab.com/YuehanLi1224/mini-proj3/-/raw/main/Screenshot%20_versioning.png?ref_type=heads)

**The bucket has encryption enabled:**
![The bucket has encryption enabled:](https://gitlab.com/YuehanLi1224/mini-proj3/-/raw/main/Screenshot_encryption.png?ref_type=heads)

**The usage of CodeWhisperer:**  

In lib/mini_proj3-stack.ts, I used // make an S3 bucket that enables versioning and encrption  

In bin/mini_proj3.ts, I used // add necessary variables so that a S3 bucket is created and is deployed correctly
